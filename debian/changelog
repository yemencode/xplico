xplico (1.2.2-0kali2) kali-dev; urgency=medium

  * Add a patch to have an explicit message about the existing script to
    download the .mmdb

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 19 Aug 2019 17:13:44 +0200

xplico (1.2.2-0kali1) kali-dev; urgency=medium

  * New upstream release.
  * Update Maintainer field
  * Update Vcs-* fields for the move to gitlab.com
  * Add GitLab's CI configuration file
  * Fix build with json-c 0.13
  * Fix typo in README.Debian
  * Improve xplico.service
  * Ignore lintian error dir-or-file-in-opt
  * Ignore missing sources lintian error
  * Ignore lintian's possible-gpl-code-linked-with-openssl as well
  * Switch to debhelper compat level 12
  * Clean up rules file
  * Disable parallel building
  * Fix upstream makefile to not fudge with the debian dir

 -- Raphaël Hertzog <raphael@offensive-security.com>  Mon, 19 Aug 2019 09:18:41 +0200

xplico (1.2.1+git20181226-0kali1) kali-dev; urgency=medium

  * Remove old useless patch
  * New upstream version 1.2.1+git20181226
  * Add a patch to find ndpi lib in debian
  * Update README.Debian

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 06 Feb 2019 10:34:25 +0100

xplico (1.2.1-0kali3) kali-dev; urgency=medium

  * Add xplico-webui & xplico-webui-stop

 -- Ben Wilson <g0tmi1k@offensive-security.com>  Fri, 02 Mar 2018 15:51:14 +0000

xplico (1.2.1-0kali2) kali-dev; urgency=medium

  * Fix issue 4560:
    - Add missing directories in /opt/xplico/xi/app/tmp/cache/
    - Import a patch for Cake to be compatible with php7
  * Add a patch to search correctly php.ini

 -- Sophie Brun <sophie@freexian.com>  Wed, 28 Feb 2018 10:42:58 +0100

xplico (1.2.1-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Install the script for GeoLite databases
  * Add a patch to improve config file

 -- Sophie Brun <sophie@freexian.com>  Thu, 23 Nov 2017 10:39:09 +0100

xplico (1.2.1) stable; urgency=low
  * Bugfix: WUI: added check in the registration page; added the file name check on the pcap upload page
 -- Gianluca Costa <g.costa@xplico.org>  Sun, 12 Nov 2017 09:10:00 +0200

xplico (1.2.0-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@freexian.com>  Mon, 16 Jan 2017 15:31:25 +0100

xplico (1.2.0) stable; urgency=low
  * Migration from PHP5 to PHP7
  * CakePHP 2.8
  * IMAP bug fix
  * Bugfix: reported on Security Onion
 -- Gianluca Costa <g.costa@xplico.org>  Sun, 15 Jan 2017 11:10:00 +0200

xplico (1.1.2-0kali4) kali-dev; urgency=medium

  * Add missing build-dep: zlib1g-dev

 -- Sophie Brun <sophie@freexian.com>  Tue, 10 Jan 2017 14:08:41 +0100

xplico (1.1.2-0kali3) kali-dev; urgency=medium

  * Fix postinst script to work with any PHP version.

 -- Raphaël Hertzog <hertzog@debian.org>  Wed, 13 Jul 2016 21:10:39 +0200

xplico (1.1.2-0kali2) kali-dev; urgency=medium

  * Update dependencies:
    - replace python3.4 with generic python3
    - replace php5-* with php-* equivalents

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 12 Jul 2016 18:30:17 +0200

xplico (1.1.2-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Refresh 0001-Use-pkgconfig-to-retrieve-the-correct-parameters-to-.patch
  * Add libpq-dev as build-depends
  * Add a patch to have hardened build LDFLAGS
  * Update debian/init.d and debian/docs

 -- Sophie Brun <sophie@freexian.com>  Thu, 21 Jan 2016 10:56:00 +0100

xplico (1.1.2) stable; urgency=low
  * IPv4 defragmentation
  * CapAnalysis dissectors and dispatcher
 -- Gianluca Costa <g.costa@xplico.org>  Sun, 10 Jan 2016 12:00:00 +0200

xplico (1.1.1-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Drop patches: 0002-Fixes-to-compile-with-libndpi-1.5.patch (useless patch)
    and 0003-Fixes-to-compile-in-kali-dev.patch (applied upstream)

 -- Sophie Brun <sophie@freexian.com>  Thu, 05 Nov 2015 11:24:21 +0100

xplico (1.1.1) stable; urgency=low
  * Whatsapp OS and Phone number
  * Added MGCP dissector
  * IMAP bug fixed
 -- Gianluca Costa <g.costa@xplico.org>  Sun, 1 Nov 2015 15:12:00 +0200

xplico (1.1.0-0kali3) kali-dev; urgency=medium

  * Rebuild because of new version libndpi (libndpi2 instead of libndpi1a)
  * Add a patch to compile in new kali-dev

 -- Sophie Brun <sophie@freexian.com>  Mon, 05 Oct 2015 11:00:34 +0200

xplico (1.1.0-0kali2) kali-dev; urgency=medium

  * Update postinst and postrm with invoke-rc.d

 -- Sophie Brun <sophie@freexian.com>  Tue, 16 Dec 2014 16:45:15 +0100

xplico (1.1.0-0kali1) kali-dev; urgency=medium

  * New upstream release (that actually builds with libndpi1a 1.5).
  * Add watch file.
  * Bump debhelper compat level to 9
  * Switch to 3.0 (quilt) source format.
  * Add autoconf to Build-Depends for autoreconf usage in
    third-party/json-c/autogen.sh.
  * Add automake to build-depends for the same reasons.
  * Add libtool to build-depends too.
  * Add patch to compile with ndpi 1.5

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 04 Sep 2014 15:49:49 +0200

xplico (1.1.0) stable; urgency=low
  * Performance improved
  * nDPI updated
  * IRC bug fixed
  * HTTP bug fixed
  * VoIP (SIP, RTP) bug fixed
  * FTP bug fixed
  * changed the FaceBook DB tables
  * Null/Loopback dissector
  * Cisco HDLC dissector
  * Libero.it and RossoAlice webmail decoding
  * Yahoo messenger (web and mobile)
  * Dig using file signatures (unknown flows)

 -- Gianluca Costa <g.costa@xplico.org>  Mon, 23 Dec 2013 14:22:00 +0200

xplico (1.0.1kali5) kali-dev; urgency=medium

  * Rebuild with libndpi1a 1.5.

 -- Raphaël Hertzog <hertzog@debian.org>  Wed, 03 Sep 2014 17:05:58 +0200

xplico (1.0.1kali4) kali-dev; urgency=medium

  * Change depends in python3 (instead of python3.2-minimal)

 -- Sophie Brun <sophie@freexian.com>  Thu, 07 Aug 2014 15:17:32 +0200

xplico (1.0.1kali3) kali; urgency=low

  * Add missing pkg-config build depends.
  * Create /opt/xplico/xi/app/tmp/cache writable by www-data.

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 09 Aug 2013 19:01:18 +0000

xplico (1.0.1kali2) kali; urgency=low

  * Clean up desktop files

 -- Mati Aharoni <muts@kali.org>  Fri, 09 Aug 2013 12:28:41 -0400

xplico (1.0.1kali1) kali; urgency=low

  * Fix bug in system/dema/session_decoding.c that made the compilation fail.
  * Update upstream Makefile to use libndpi as a shared library available in
    the system path.
  * Crude update of tcp_grbg & udp_grbg dissectors to work with latest
    nDPI.
  * Use xdg-open instead of exo-open in desktop file (and depend on
    xdg-utils).
  * Add missing sudo dependency.

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 09 Aug 2013 13:39:48 +0000

xplico (1.0.1) stable; urgency=low
  * nDPI integration
  * performance improved
  * FTP dissector improved
  * Added the prism dissector
  * CLI execution bug fixed
  * PCAP-over-IP SSL encryption
  * IRC dissector improvements
  * File reconstruction from Fragmented Payloads improved
  * FaceBook Chat updated
  * FaceBook Message (partial)
  * HTTP without initial packets (packets lost)
  * RTP dissector improved
  * PCAP2WAV, RTP2WAV interface added

 -- Gianluca Costa <g.costa@xplico.org>  Sun, 16 Sep 2012 17:57:23 +0200


xplico (1.0.0) stable; urgency=low
  * added the PPI dissector
  * added the syslog dissector
  * added "Bogus IP length" correction with checksum verification disabled
  * new Facebook Chat dissector for the new Facebook chat protocol
  * SIP dissector improved
  * IMAP dissector improved and bugs fixed
  * DNS dissector PIPI improved
  * Yahoo Webmail bugs fixed
  * Live/Hotmail WebMail Spanish version
  * GeoMap improved
  * PCap-over-IP

 -- Gianluca Costa <g.costa@xplico.org>  Sun, 12 Feb 2012 10:57:23 +0200


xplico (0.7.1) stable; urgency=low
  * RTP bug fixed
  * dispatcher core functionality bug fixed
  * mfile manipulator bug fixed
  * XI bugs fixed
  * added DB migration tool

 -- Gianluca Costa <g.costa@xplico.org>  Mon, 03 Oct 2011 10:57:23 +0200


xplico (0.7.0) stable; urgency=low
  * upgraded XI to Cakephp 1.3
  * added the ICMPv6 dissector
  * Ethernet dissector improved (necessary for ICMPv6)
  * deadlock fixed
  * fixed the communication bug from xplico and manipulators
  * SDP dissector bug fixes
  * SIP and TCP dissectors improved
  * WebMail manipulator and all python3 scripts improved (ready to new webmail entry... see pol ;) )
  * added pcap file name on CLI report screen
  * capture modules log improved
  * new GeoIP version: 1.4.8
  * added IPv6 Hop-by-Hop Options
  * Xplico and all Manipulators with dual stack (IPv4, IPv6)
  * XI language localization
  * DNS bugfix
  * added the  MDNS dissector
  * added AOL WebMail
  * added Yahoo! WebMail
  * added Yahoo! Mail for Andorid Mobile
  * added Gmail

 -- Gianluca Costa <g.costa@xplico.org>  Sun, 30 Oct 2011 10:57:23 +0200


xplico (0.6.3) stable; urgency=low
  * xplico 32/64bit version
  * new decoding manager (DeMa) version 0.3.1
  * mfile manipulator (HTTP file transfare) bug fixes
  * WebMail scripts improved
  * HTTP dissector improved
  * upgraded the XI javascript libraries

 -- Gianluca Costa <g.costa@xplico.org>  Wed, 1 Jun 2011 10:57:23 +0200

xplico (0.6.2) stable; urgency=low
  * l7-patterns for all flows/protocols not decoded by xplico
  * Xplico Interface (XI) improved
  * python3 porting of many script
  * realtime capture module improved
  * facebook chat realtime view
  * UTC/localtime bug fixes
  * l2tp dissector bug fixes
  * cli and lite dispatchers bug fixes
  * telnet dissector bug fixes

 -- Gianluca Costa <g.costa@xplico.org>  Sat, 30 Apr 2011 11:36:50 +0200

xplico (0.6.1) stable; urgency=low
  * Paltalk dissector
  * MSN basic dissector (beta)
  * XI bugfix
  * XI: Cookie hijacking
  * XI: XSS fixed
  * XI: Imges and Webs pages improved

 -- Gianluca Costa <g.costa@xplico.org>  Sat, 20 Nov 2010 07:28:40 +0200

xplico (0.6.0) stable; urgency=low
  * XI configuration pages
  * XI administator pages
  * XI multi-user
  * IRC dissector
  * ARP/RAP dissector
  * radiotap dissector
  * latitude, longitude selectable from XI
  * CLI decoding directory (xdecode) selectable
  * Telent dissector with PIPI
  * Paltalk Express dissector and aggregator (basic version)
  * sftp/scp pcap files upload

 -- Gianluca Costa <g.costa@xplico.org>  Sun, 3 Oct 2010 13:28:40 +0200


xplico (0.5.8) stable; urgency=low
  * RTP, FTP, Telnet, SIP  dissector improvement
  * RTP bug fix
  * XI XSS fixed
  * CakePHP 1.2.7
  * new tool to manage pcap named trigcap
  * videosnarf 0.63

 -- Gianluca Costa <g.costa@xplico.org>  Mon, 28 Jun 2010 08:16:24 +0200


xplico (0.5.7) stable; urgency=low
  * RTCP dissector
  * RTP dissector improvement
  * SIP dissector improvement

 -- Gianluca Costa <g.costa@xplico.org>  Fri, 30 Apr 2010 20:22:24 +0200

xplico (0.5.6) stable; urgency=low
  * Pass from UTC capture time to local time
  * HTTP file reconstruction
  * garbage dissectors with text search
  * RTP dissector with audio decoding (by videosnarf)
  * SIP dissector with audio decoding (by videosnarf)

 -- Gianluca Costa <g.costa@xplico.org>  Mon, 19 Apr 2010 21:29:44 +0200

xplico (0.5.5) stable; urgency=low
  * migrating to SQLite3
  * telnet dissector
  * web mail dissector (AOL, YAHOO!, Live, AOL)
  * web mail manipulator (YAHOO!, Live, AOL): mail without attach
  * LLC dissector improving
  * XI improving
  * script to check for new release

 -- Gianluca Costa <g.costa@xplico.org>  Sun, 21 Feb 2010 20:13:11 +0200

xplico (0.5.4) stable; urgency=low

  * Facebook chat
  * CakePHP 1.2.x

 -- Gianluca Costa <g.costa@xplico.org>  Tue, 22 Dec 2009 19:56:41 +0200

xplico (0.5.3) stable; urgency=low

  * Xplico DEFT Edition

 -- Gianluca Costa <g.costa@xplico.org>  Tue, 27 Oct 2009 20:41:56 +0200


xplico (0.5.2) stable; urgency=low

  * Ubuntu first release.

 -- Gianluca Costa <g.costa@xplico.org>  Wed, 05 Aug 2009 20:41:56 +0200
